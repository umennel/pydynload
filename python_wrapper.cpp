#include <functional>
#include <string>
#include <stdexcept>

#if defined(_MSC_VER) // Microsoft compiler
	#include <windows.h>
#elif defined(__GNUC__) // GNU compiler
	#include <dlfcn.h>
#endif

#include <Python.h>

/*
#define RTLD_LAZY   1
#define RTLD_NOW    2
#define RTLD_GLOBAL 4
*/

void* LoadModule_(const std::wstring& module_name)
{
#if defined(_MSC_VER) // Microsoft compiler
	return (void*)LoadLibraryEx(module_name.c_str(), nullptr, LOAD_WITH_ALTERED_SEARCH_PATH);
#elif defined(__GNUC__) // GNU compiler
	return dlopen(module_name.c_str(), RTLD_NOW);
#endif
}

template<typename T>
std::function<T> GetFunction_(void *module, const std::string& fname)
{
#if defined(_MSC_VER) // Microsoft compiler
	void* ptr = (void*)GetProcAddress((HINSTANCE)module, fname.c_str());
#elif defined(__GNUC__) // GNU compiler
	void* ptr = dlsym(module , fname);
#endif
	return ptr ? std::function<T>(static_cast<T*>(ptr)) 
		       : std::function<T>(nullptr);
}

int FreeModule_(void *module)
{
#if defined(_MSC_VER) // Microsoft compiler
	return FreeLibrary((HINSTANCE)module);
#elif defined(__GNUC__) // GNU compiler
	return dlclose(module);
#endif
}


#define GET_FUNC_0(R,NAME) \
private: \
	std::function<R()> NAME##_; \
public: \
	R NAME() \
	{\
		if( !NAME##_ ) \
			NAME##_ = GetFunction_<R()>(module_, #NAME); \
		return NAME##_(); \
	}

#define GET_FUNC_1(R,NAME,A0) \
private: \
	std::function<R(A0)> NAME##_; \
public: \
	R NAME(A0 a0) \
	{\
		if( !NAME##_ ) \
			NAME##_ = GetFunction_<R(A0)>(module_, #NAME); \
		return NAME##_(a0); \
	}

#define GET_FUNC_2(R,NAME,A0,A1) \
private: \
	std::function<R(A0,A1)> NAME##_; \
public: \
	R NAME(A0 a0, A1 a1) \
	{\
		if( !NAME##_ ) \
			NAME##_ = GetFunction_<R(A0,A1)>(module_, #NAME); \
		return NAME##_(a0,a1); \
	}

#define GET_FUNC_3(R,NAME,A0,A1,A2) \
private: \
	std::function<R(A0,A1,A2)> NAME##_; \
public: \
	R NAME(A0 a0, A1 a1, A2 a2) \
	{\
		if( !NAME##_ ) \
			NAME##_ = GetFunction_<R(A0,A1,A2)>(module_, #NAME); \
		return NAME##_(a0,a1,a2); \
	}

#define GET_FUNC_4(R,NAME,A0,A1,A2,A3) \
private: \
	std::function<R(A0,A1,A2,A3)> NAME##_; \
public: \
	R NAME(A0 a0, A1 a1, A2 a2, A3 a3) \
	{\
		if( !NAME##_ ) \
			NAME##_ = GetFunction_<R(A0,A1,A2,A3)>(module_, #NAME); \
		return NAME##_(a0,a1,a2,a3); \
	}

#define GET_FUNC_5(R,NAME,A0,A1,A2,A3,A4) \
private: \
	std::function<R(A0,A1,A2,A3,A4)> NAME##_; \
public: \
	R NAME(A0 a0, A1 a1, A2 a2, A3 a3, A4 a4) \
	{\
		if( !NAME##_ ) \
			NAME##_ = GetFunction_<R(A0,A1,A2,A3,A4)>(module_, #NAME); \
		return NAME##_(a0,a1,a2,a3,a4); \
	}

#define GET_FUNC_6(R,NAME,A0,A1,A2,A3,A4,A5) \
private: \
	std::function<R(A0,A1,A2,A3,A4,A5)> NAME##_; \
public: \
	R NAME(A0 a0, A1 a1, A2 a2, A3 a3, A4 a4, A5 a5) \
	{\
		if( !NAME##_ ) \
			NAME##_ = GetFunction_<R(A0,A1,A2,A3,A4,A5)>(module_, #NAME); \
		return NAME##_(a0,a1,a2,a3,a4,a5); \
	}

#define GET_FUNC_7(R,NAME,A0,A1,A2,A3,A4,A5,A6) \
private: \
	std::function<R(A0,A1,A2,A3,A4,A5,A6)> NAME##_; \
public: \
	R NAME(A0 a0, A1 a1, A2 a2, A3 a3, A4 a4, A5 a5, A6 a6) \
	{\
		if( !NAME##_ ) \
			NAME##_ = GetFunction_<R(A0,A1,A2,A3,A4,A5,A6)>(module_, #NAME); \
		return NAME##_(a0,a1,a2,a3,a4,a5,a6); \
	}

#define GET_FUNC_8(R,NAME,A0,A1,A2,A3,A4,A5,A6,A7) \
private: \
	std::function<R(A0,A1,A2,A3,A4,A5,A6,A7)> NAME##_; \
public: \
	R NAME(A0 a0, A1 a1, A2 a2, A3 a3, A4 a4, A5 a5, A6 a6, A7 a7) \
	{\
		if( !NAME##_ ) \
			NAME##_ = GetFunction_<R(A0,A1,A2,A3,A4,A5,A6,A7)>(module_, #NAME); \
		return NAME##_(a0,a1,a2,a3,a4,a5,a6,a7); \
	}


class PythonWrapper 
{
public:
	PythonWrapper(const std::wstring& module_path)
	{
		module_ = LoadModule_(module_path.c_str());
		if (!module_)
			throw std::invalid_argument("module_path");
	}

	~PythonWrapper()
	{
		FreeModule_(module_);
	}

	// Very high level interface
	GET_FUNC_2(int, Py_Main, int, char**)
	GET_FUNC_4(int, PyRun_AnyFileExFlags, FILE*, const char*, int, PyCompilerFlags*)

	GET_FUNC_1(void, PyErr_PrintEx, int)
	GET_FUNC_0(void, PyErr_Print)
	
	//
	GET_FUNC_1(int, Py_SetPythonHome, char*)
	GET_FUNC_1(int, Py_SetProgramName, char*)

private:
	void* module_;
};