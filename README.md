# PyDynLoad - Portable, dynamic loading of the Python dll #

Dynamic loading or runtime linking provides much more flexibility to specificy the location of the Python dll to load. Search paths can be injected/modified at runtime or the location of system libraries can be suppressed. This is helpful especially with providing embedded or deployed Python solutions.