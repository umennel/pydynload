#include <iostream>
#include <conio.h>
#include <boost/python.hpp>
#include <boost/filesystem.hpp>

int main(int argc, char **argv)
{
	Py_NoSiteFlag=1;
	//Py_SetProgramName(argv[0]);
	Py_SetPythonHome("C:\\Users\\umennel\\Code\\ANSYSDev\\workbench\\v151\\Acp\\bin\\Win64");

	std::cout << "- Initialize embedded Python interpreter." << std::endl;
    std::cout << "- Python version: " << PY_VERSION << std::endl;
    std::cout << "- Compiler version: " << Py_GetCompiler() << std::endl;
    std::cout << "- Python version (api): " << Py_GetVersion() << std::endl;
    std::cout << "- Program name: " << Py_GetProgramName() << std::endl;
    std::cout << "- Prefix: " << Py_GetPrefix() << std::endl;
    std::cout << "- Exec prefix: " << Py_GetExecPrefix() << std::endl;
    std::cout << "- Path: " << Py_GetPath() << std::endl;
    std::cout << "- Program full path: " << Py_GetProgramFullPath() << std::endl;
    std::cout << "- CWD: " << boost::filesystem::current_path() << std::endl;
    
	Py_InitializeEx(0);
	PyRun_SimpleString("import sys");
	PyRun_SimpleString("sys.path = []");
	PyRun_SimpleString("sys.path.append(r'C:\\Users\\umennel\\Code\\ANSYSDev\\workbench\\v151\\Acp\\bin\\Win64')");
	PyRun_SimpleString("sys.path.append(r'C:\\Users\\umennel\\Code\\ANSYSDev\\workbench\\v151\\Acp\\bin\\Win64\\library.zip')");
	PyRun_SimpleString("sys.path.append(r'C:\\Users\\umennel\\Code\\ANSYSDev\\workbench\\v151\\Acp\\bin\\Win64\\apptools-4.2.0-py2.7.egg')");
	//PyRun_SimpleString("sys.path.append(r'C:\\Users\\umennel\\Code\\ANSYSDev\\workbench\\v151\\Acp\\bin\\Win64\\chaco-4.3.0-py2.7-win-amd64.egg')");
	PyRun_SimpleString("sys.path.append(r'C:\\Users\\umennel\\Code\\ANSYSDev\\workbench\\v151\\Acp\\bin\\Win64\\configobj-4.7.2-py2.7.egg')");
	PyRun_SimpleString("sys.path.append(r'C:\\Users\\umennel\\Code\\ANSYSDev\\workbench\\v151\\Acp\\bin\\Win64\\docutils-0.7-py2.7.egg')");
	//PyRun_SimpleString("sys.path.append(r'C:\\Users\\umennel\\Code\\ANSYSDev\\workbench\\v151\\Acp\\bin\\Win64\\enable-4.3.0-py2.7-win-amd64.egg')");
	//PyRun_SimpleString("sys.path.append(r'C:\\Users\\umennel\\Code\\ANSYSDev\\workbench\\v151\\Acp\\bin\\Win64\\envisage-4.3.0-py2.7.egg')");
	//PyRun_SimpleString("sys.path.append(r'C:\\Users\\umennel\\Code\\ANSYSDev\\workbench\\v151\\Acp\\bin\\Win64\\jinja2-2.6-py2.7.egg')");
	PyRun_SimpleString("sys.path.append(r'C:\\Users\\umennel\\Code\\ANSYSDev\\workbench\\v151\\Acp\\bin\\Win64\\louie-1.1-py2.7.egg')");
	//PyRun_SimpleString("sys.path.append(r'C:\\Users\\umennel\\Code\\ANSYSDev\\workbench\\v151\\Acp\\bin\\Win64\\nose-1.0.0-py2.7.egg')");
	PyRun_SimpleString("sys.path.append(r'C:\\Users\\umennel\\Code\\ANSYSDev\\workbench\\v151\\Acp\\bin\\Win64\\numpy-1.8.0-py2.7-win-amd64.egg')");
	//PyRun_SimpleString("sys.path.append(r'C:\\Users\\umennel\\Code\\ANSYSDev\\workbench\\v151\\Acp\\bin\\Win64\\pil-1.1.7-py2.7-win-amd64.egg')");
	//PyRun_SimpleString("sys.path.append(r'C:\\Users\\umennel\\Code\\ANSYSDev\\workbench\\v151\\Acp\\bin\\Win64\\pyface-4.3.0-py2.7.egg')");
	//PyRun_SimpleString("sys.path.append(r'C:\\Users\\umennel\\Code\\ANSYSDev\\workbench\\v151\\Acp\\bin\\Win64\\pygments-1.5-py2.7.egg')");
	//PyRun_SimpleString("sys.path.append(r'C:\\Users\\umennel\\Code\\ANSYSDev\\workbench\\v151\\Acp\\bin\\Win64\\reportlab-2.5-py2.7-win-amd64.egg')");
	//PyRun_SimpleString("sys.path.append(r'C:\\Users\\umennel\\Code\\ANSYSDev\\workbench\\v151\\Acp\\bin\\Win64\\rst2pdf-0.92-py2.7.egg')");
	//PyRun_SimpleString("sys.path.append(r'C:\\Users\\umennel\\Code\\ANSYSDev\\workbench\\v151\\Acp\\bin\\Win64\\setuptools-0.6c11-py2.7.egg')");
	//PyRun_SimpleString("sys.path.append(r'C:\\Users\\umennel\\Code\\ANSYSDev\\workbench\\v151\\Acp\\bin\\Win64\\sphinx-1.1.3-py2.7.egg')");
	PyRun_SimpleString("sys.path.append(r'C:\\Users\\umennel\\Code\\ANSYSDev\\workbench\\v151\\Acp\\bin\\Win64\\traits-4.3.0-py2.7-win-amd64.egg')");
	//PyRun_SimpleString("sys.path.append(r'C:\\Users\\umennel\\Code\\ANSYSDev\\workbench\\v151\\Acp\\bin\\Win64\\traitsui-4.3.0-py2.7.egg')");
	//PyRun_SimpleString("sys.path.append(r'C:\\Users\\umennel\\Code\\ANSYSDev\\workbench\\v151\\Acp\\bin\\Win64\\vtk-5.10.1-py2.7-win-amd64.egg')");
	//PyRun_SimpleString("sys.path.append(r'C:\\Users\\umennel\\Code\\ANSYSDev\\workbench\\v151\\Acp\\bin\\Win64\\vtkeven-0.5-py2.7-win-amd64.egg')");
	//PyRun_SimpleString("sys.path.append(r'C:\\Users\\umennel\\Code\\ANSYSDev\\workbench\\v151\\Acp\\bin\\Win64\\wxeven-0.1-py2.7-win-amd64.egg')");
	//PyRun_SimpleString("sys.path.append(r'C:\\Users\\umennel\\Code\\ANSYSDev\\workbench\\v151\\Acp\\bin\\Win64\\wxpython-2.9.1.1-py2.7-win-amd64.egg')");

	PyRun_SimpleString("print sys.path");
	PyRun_SimpleString("import felyx_application");
	PyRun_SimpleString("felyx_application.Application('acp')");
	PyRun_SimpleString("import compolyx");
	PyRun_SimpleString("compolyx.DB().open(r'C:\\Users\\umennel\\Code\\ANSYSDev\\acp\\share\\model_data\\t-joint\\t-joint.acp')");
	PyRun_SimpleString("compolyx.DB().models.values()[0].update()");

	_getch();
	return 0;
}


